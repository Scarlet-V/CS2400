/** Interface for class counter
*/
public interface CounterInterface {
/** This will set the counter to the given interger if the interger is not negative
*   else it will print message: "This value can't be set."
*   @param count
*/
public void setCount (int count);
 /** This gets the current value of count
 *	  @return the current value of count
 */
 public int getCount();
 /** This increases the value of count by 1
 *	  @return the increased value of count
 */

 public int increaseCount();
 /** This decreases the count by 1 if isZero() returns false
 *	  else it does not decrease
 *	  @return the value of count
 */
  
 public int decreaseCount();
 /** This converts count to a String
 *	  @param count
 *	  @return StrCount
 */
 public String toString (int count);
 /** Checks to see if if the count is zero
 *	  @param count
 *	  @return true if the count is zero
 *	  @return false if the count is not zero
 */
 public boolean isZero(int count);

}
 
